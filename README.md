Instructions
---
To view the architecture diagram, open it with draw.io.


Overview
---
This was designed with taking the necessity of a `201` status code into account. Rather than uploading, receiving an immediate `202`, waiting for the task to process while polling, and finally receiving a `303`, I had to look at the semantics of the `201` status code and noted that it required immediate creation of a resource. That being said, I chose to create a task with my `201`.

As it stands, here is the proposed logic, from the client's point of view:
1. Client POSTs to upload API endpoint. A task is created by the backend and inserted into the task queue, and a `201` response is generated with the URI of that task's status page as its `Location` header.
2. Client polls task status page. Task status page JSON content changes as the image resize task is processed, but the response should always be `200`. Client continues to retry polling until task completion is registered.
3. Once the task has reported back its completion, the task status page has an array of URIs pertaining to the specific locations of the generated thumbnails.

From the server's perspective:
1. Client uploads to upload API endpoint.
2. A task is created and inserted into the task queue, using the SHA1 of the image file as its ID and containing the entire image as bytes. That way, retention of the original image is directly related to the retention time of the queue.
3. A record is inserted into the key/value store, with the record's key being a SHA1 hash of the image file's contents. The values track things like submitter information, timing information, and task status.
4. Once the job is done, the record in the key-value store will be updated, a callback will be issued to the notification gateway, and the thumbnails will be sent to some storage system, where they can be served by a basic webserver/cached by a CDN/otherwise.


Pros and Cons
---
Pros:
- Modular
  - Webservers and image processors are completely independently scalable
  - Failures are contained to the failing component
- Utilize (possibly) existing infrastructure
- Fully asynchronous
- Extensible (task processing concept can be used for many things)

Cons:
- Complexity
  - large footprint (may even be overbuilt)
  - lots of moving parts, all of which can break
- Understanding of multiple types of system is necessary
- Likely very network-inefficient at this point, could use refining.


Bottlenecks
---
Overall, I think this system should be able to scale pretty well, but there are definitely some areas of optimization/better architecting possible.
- Task message size will be large due to image data encapsulated within
  - could write out the image immediately upon upload and then reference it to the task processor via URL or otherwise
- K/V store needs to be able to handle the volume of incoming items
- Task processors must be able to keep up with the volume of tasks going into the queue, otherwise the queue may fill
- Storage and retention will almost certainly be a problem here
  - compliance
  - retention time & data storage size
    - assuming 500KB of thumbnails created at a time, 100 requests is around 50MB
    - 100 requests per second all day long would mean 4.32TB per day just in image thumbnails, that doesn't account for total bandwidth or anything else
  - one way to address this would be to not have any retention at all (only generate on-demand, images would have an extremely short lifecycle)
- The static asset webserver may become overloaded, so I've specified it acting as origin for a CDN
- We need a way of dealing with bad data
- Network could bottleneck us with the kind of throughput that this system can handle


Outages
---
- If the task queue dies, previously-submitted images will still be usable. No new images can be processed.
  - Generally speaking, it should be able to read its backlog and know where it needs to restart from.
- If the K/V store dies, same story. Tasks will likely start failing and may need to be resubmitted
  - Might be able to add some kind of recovery mode here based off of the status of a given item in the store
- If the task queue doesn't have things being pulled from it fast enough, it will fill and new submissions may fail
  - queue filling is not really recoverable, but this is easily detectable and preventable
-  The asset serving infrastructure is mostly separated from the processing infrastructure


Monitoring
---
- Quite a few things are available here, and this is just a small selection - ultimately, we need to detect any of the above outage modes via our monitoring
- Queue size, processing time, processes per time period, failures per time period, queue partitioning/rebalancing, K/V store partitioning/rebalancing, disk space for retention, key/value insert/update rate, etc.


Other Architecture Notes
---
- An example of a task queue system that would work for this design is Kafka
- An example of a K/V store system that would work for this design is Memcached
- K/V store is clustered, partitioned by hash of key
- HA loadbalancing is achieved through keepalived/ipvs/BGP - we need to be able to lose a loadbalancer or two
- Asset storage would be accessible via multiple protocols including HTTP; asset storage would ideally be active/active, but possibly active/passive (or utilize a hot spare, or maybe offsite backup) - as noted earlier the storage/retention question is a very large one
- Could use a good system for graceful degradation